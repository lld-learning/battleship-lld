import org.ashish.tech.Main;
import org.ashish.tech.io.ConsoleInput;
import org.ashish.tech.io.ConsoleOutput;
import org.ashish.tech.models.Board;
import org.ashish.tech.models.BoardPiece;
import org.ashish.tech.models.Player;
import org.ashish.tech.models.boundary.RectangularBoundary;
import org.ashish.tech.service.GameService;
import org.ashish.tech.strategies.DefaultGameWinningStrategy;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GameTest {


    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    @Before
    public void setup(){
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void gameLoop() throws IOException {
        final String commands = "2 1 1\r\n" + "exit\r\n";
        final String expectedOutput =
                "Welcome to BattleShip Game\n" +
                        "Player Ashish : Enter your move \n" +
                        "Player Ashish move was a hit\n" +
                        "Ashish has won the game\n";

        final ByteArrayInputStream in = new ByteArrayInputStream(commands.getBytes());
        System.setIn(in);

        Main.main(new String[] {});
        assertEquals(expectedOutput, outContent.toString());
    }
}
