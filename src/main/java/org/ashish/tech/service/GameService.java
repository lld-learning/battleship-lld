package org.ashish.tech.service;

import org.ashish.tech.io.IExternalInput;
import org.ashish.tech.io.IExternalOutput;
import org.ashish.tech.models.Player;
import org.ashish.tech.models.PlayerMove;
import org.ashish.tech.models.PlayerMoveResult;
import org.ashish.tech.strategies.IGameWinningStrategy;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class GameService {

    private final List<Player> players;
    private final IExternalInput externalInput;
    private final IExternalOutput externalOutput;
    private final IGameWinningStrategy gameWinningStrategy;
    private int currentPlayerIndex=0;

    public GameService(List<Player> players, IExternalInput externalInput, IExternalOutput externalOutput, IGameWinningStrategy gameWinningStrategy) {
        this.players = players;
        this.externalInput = externalInput;
        this.externalOutput = externalOutput;
        this.gameWinningStrategy = gameWinningStrategy;
    }

    public void startGame() throws IOException {
        Player selectedPlayer=players.get(currentPlayerIndex);
        while(true){

            externalOutput.print("Player "+ selectedPlayer.getPlayerProfile().getName()
                    + " : Enter your move ");

            PlayerMove playerMove=selectedPlayer.getPlayerMove(players,externalInput.takeInput());
            PlayerMoveResult playerMoveResult=playerMove.getTargetPlayer().takeHit(playerMove.getHitCoordinates());

            externalOutput.print("Player "+ selectedPlayer.getPlayerProfile().getName()
                    + " move was a "
                    + (playerMoveResult.isHit()?"hit":"miss"));

            Optional<Player> winner=gameWinningStrategy.hasGameBeenWon(players);
            if(winner.isPresent()){
                externalOutput.print(winner.get().getPlayerProfile().getName()+ " has won the game");
                break;
            }
            currentPlayerIndex=(currentPlayerIndex+1)%players.size();
            selectedPlayer=players.get(currentPlayerIndex);
        }
    }
}
