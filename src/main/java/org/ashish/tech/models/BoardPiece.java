package org.ashish.tech.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.ashish.tech.models.boundary.IBoundary;

@Getter
@AllArgsConstructor
public class BoardPiece {
    private final String name;
    private final IBoundary boundary;

}
