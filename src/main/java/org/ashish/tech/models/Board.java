package org.ashish.tech.models;

import org.ashish.tech.models.boundary.IBoundary;

import java.util.*;

public class Board {
    private final List<Coordinates> bombardedCoordinates;
    private final List<BoardPiece> boardPieces;

    private final Set<BoardPiece> killedBoardPieces;
    private final IBoundary boundary;

    public Board(List<BoardPiece> boardPieces, IBoundary boundary) {
        this.boardPieces = boardPieces;
        this.boundary = boundary;
        this.bombardedCoordinates = new ArrayList<>();
        this.killedBoardPieces = new HashSet<>();
    }

    public PlayerMoveResult takeHit(Coordinates coordinates){
        if(boundary.containsCoordinates(coordinates)){
            bombardedCoordinates.add(coordinates);
        }
        Optional<BoardPiece> boardPieceHit=boardPieceHit(coordinates);
        boardPieceHit.ifPresent(killedBoardPieces::add);

        return boardPieceHit.isPresent()?PlayerMoveResult.hit():PlayerMoveResult.miss();
    }

    private Optional<BoardPiece> boardPieceHit(Coordinates coordinates){
        return boardPieces.stream().filter(b -> b.getBoundary().containsCoordinates(coordinates)).findAny();
    }

    public boolean areAllPiecesKilled(){
        return killedBoardPieces.size()== boardPieces.size();
    }
}
