package org.ashish.tech.models.boundary;

import org.ashish.tech.models.Coordinates;

public class RectangularBoundary implements IBoundary{

    private final int x1;
    private final int y1;
    private final int x2;
    private final int y2;

    public RectangularBoundary(int x1,
                               int y1,
                               int x2,
                               int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public boolean containsCoordinates(Coordinates coordinates) {
        return (coordinates.getX()>=x1 && coordinates.getX()<=x2 && coordinates.getY()>=y1 && coordinates.getY()<=y2);
    }
}
