package org.ashish.tech.models.boundary;

import org.ashish.tech.models.Coordinates;

public interface IBoundary {
    boolean containsCoordinates(Coordinates coordinates);
}
