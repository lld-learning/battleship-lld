package org.ashish.tech.models;

import lombok.Getter;

@Getter
public class Coordinates {
    private int x;
    private int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
