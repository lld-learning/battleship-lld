package org.ashish.tech.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerProfile {

    private String name;

    public PlayerProfile(String name) {
        this.name = name;
    }
}
