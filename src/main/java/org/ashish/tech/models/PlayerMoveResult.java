package org.ashish.tech.models;

import lombok.Getter;

public class PlayerMoveResult {
    @Getter
    private boolean hit;

    public static PlayerMoveResult hit(){
        PlayerMoveResult p=new PlayerMoveResult();
        p.hit=true;
        return p;
    }

    public static PlayerMoveResult miss(){
        PlayerMoveResult p=new PlayerMoveResult();
        p.hit=false;
        return p;
    }
}
