package org.ashish.tech.models;

import lombok.Getter;

@Getter
public class PlayerMove {
    private final Player targetPlayer;
    private final Coordinates hitCoordinates;

    public PlayerMove(Player targetPlayer, Coordinates hitCoordinates) {
        this.targetPlayer = targetPlayer;
        this.hitCoordinates = hitCoordinates;
    }
}
