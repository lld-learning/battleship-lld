package org.ashish.tech.models;

import lombok.Getter;
import lombok.NonNull;
import org.ashish.tech.exceptions.InvalidPlayerSelectedException;
import org.ashish.tech.io.PlayerInput;

import java.util.List;
import java.util.Optional;

@Getter
public class Player {
    private final long id;
    private PlayerProfile playerProfile;
    private final Board board;

    public Player(final long id, @NonNull final Board board,PlayerProfile playerProfile) {
        this.id = id;
        this.board = board;
        this.playerProfile=playerProfile;
    }

    public PlayerMove getPlayerMove(List<Player> playerList, PlayerInput playerInput){
        Optional<Player> targetPlayer=playerList.stream().filter(p->p.getId()== playerInput.getPlayerNum()).findAny();
        if(targetPlayer.isEmpty())
            throw new InvalidPlayerSelectedException();
        return new PlayerMove(targetPlayer.get(),
                new Coordinates(playerInput.getTargetX(), playerInput.getTargetY()));
    }

    public PlayerMoveResult takeHit(@NonNull final Coordinates coordinates){
        return board.takeHit(coordinates);
    }
}
