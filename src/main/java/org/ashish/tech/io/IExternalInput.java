package org.ashish.tech.io;

import org.ashish.tech.models.Player;

import java.io.IOException;

public interface IExternalInput {

    PlayerInput takeInput() throws IOException;
}
