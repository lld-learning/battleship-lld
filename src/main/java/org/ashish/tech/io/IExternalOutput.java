package org.ashish.tech.io;

public interface IExternalOutput {

    void print(String message);
}
