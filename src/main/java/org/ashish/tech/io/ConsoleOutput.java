package org.ashish.tech.io;

public class ConsoleOutput implements IExternalOutput{
    @Override
    public void print(String message) {
        System.out.println(message);
    }
}
