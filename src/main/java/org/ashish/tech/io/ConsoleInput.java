package org.ashish.tech.io;

import org.ashish.tech.exceptions.InvalidInputException;
import org.ashish.tech.models.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleInput implements IExternalInput{
    @Override
    public PlayerInput takeInput() throws IOException {

        final InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        final BufferedReader br = new BufferedReader(inputStreamReader);
        final String line = br.readLine();

        String[] s = line.split(" ");
        if (s.length != 3) {
            throw new InvalidInputException();
        }

        final int playerNum = Integer.parseInt(s[0]);
        final int targetX = Integer.parseInt(s[1]);
        final int targetY = Integer.parseInt(s[2]);
        return new PlayerInput(playerNum,targetX,targetY);
    }
}
