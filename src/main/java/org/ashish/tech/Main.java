package org.ashish.tech;

import org.ashish.tech.io.ConsoleInput;
import org.ashish.tech.io.ConsoleOutput;
import org.ashish.tech.models.Board;
import org.ashish.tech.models.BoardPiece;
import org.ashish.tech.models.Player;
import org.ashish.tech.models.PlayerProfile;
import org.ashish.tech.models.boundary.RectangularBoundary;
import org.ashish.tech.service.GameService;
import org.ashish.tech.strategies.DefaultGameWinningStrategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        GameService gameService;
        System.out.println("Welcome to BattleShip Game");

        List<Player> playerList=new ArrayList<>();
        List<BoardPiece> boardPiecesPlayer1=new ArrayList<>();
        BoardPiece b1=new BoardPiece("ship",new RectangularBoundary(0,0,1,1));
        boardPiecesPlayer1.add(b1);
        Player player1=new Player(1,
                new Board(boardPiecesPlayer1,new RectangularBoundary(0,0,5,5)),new PlayerProfile("Ashish"));

        playerList.add(player1);

        List<BoardPiece> boardPiecesPlayer2=new ArrayList<>();
        BoardPiece b2=new BoardPiece("ship",new RectangularBoundary(0,0,1,1));
        boardPiecesPlayer2.add(b1);
        Player player2=new Player(2,new Board(boardPiecesPlayer2,new RectangularBoundary(0,0,5,5)),
                new PlayerProfile("Sosa"));

        playerList.add(player2);

        gameService=new GameService(playerList,new ConsoleInput(),new ConsoleOutput(),new DefaultGameWinningStrategy());

        gameService.startGame();
    }
}