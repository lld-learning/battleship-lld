package org.ashish.tech.strategies;

import org.ashish.tech.models.Player;

import java.util.List;
import java.util.Optional;

public interface IGameWinningStrategy {
    Optional<Player> hasGameBeenWon(List<Player> playersList);
}
