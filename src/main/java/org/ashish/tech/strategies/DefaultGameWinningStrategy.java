package org.ashish.tech.strategies;

import org.ashish.tech.models.Player;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public class DefaultGameWinningStrategy implements IGameWinningStrategy{
    @Override
    public Optional<Player> hasGameBeenWon(List<Player> playersList) {
        int enemiesToDefeat=playersList.size()-1;

        if(playersList.stream().filter(p->p.getBoard().areAllPiecesKilled()).count()==enemiesToDefeat)
            return playersList.stream().filter(p->!p.getBoard().areAllPiecesKilled()).findAny();
        else
            return Optional.empty();
    }
}
